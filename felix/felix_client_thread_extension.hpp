#pragma once

#include <memory>

#include "felix/felix_client_thread_interface.hpp"

class FelixClientThreadExtension : public FelixClientThreadInterface {

public:
    virtual ~FelixClientThreadExtension() {};

    virtual void init_subscribe(uint64_t fid) = 0;

    /**
     * @brief Function to establish a connection before sending a message.
     * 
     * This function is used to establish a connection before sending data.
     * This function needs to be called before send_data_nb(), as send_data_nb()
     * returns whether the send operaion succeeds or not.
     * The success of this call is acknowledged by the 
     * on_connection_established() callback for the particular fid.
     *  
     * @param fids The fid for which a connection needs to be established.
     * 
     * @throws FelixClientException if the connection cannot be established.
     */
    virtual void init_send_data(uint64_t fid) = 0;

    // for felix-register
    enum Cmd {
        UNKNOWN = 0,
        NOOP = 1,
        GET = 2,
        SET = 3,
        TRIGGER = 4,
        ECR_RESET = 5
    };

    enum Status {
        OK = 0,
        ERROR = 1,
        ERROR_NO_SUBSCRIPTION = 2,
        ERROR_NO_CONNECTION = 3,
        ERROR_NO_REPLY = 4,
        ERROR_INVALID_CMD = 5,
        ERROR_INVALID_ARGS = 6,
        ERROR_INVALID_REGISTER = 7,
        ERROR_NOT_AUTHORIZED = 8
    };

    struct Reply {
        uint64_t ctrl_fid;
        Status status;
        uint64_t value;
        std::string message;
    };
};
