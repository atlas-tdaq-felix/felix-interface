#pragma once

#include <memory>
#include <string>
#include <vector>

#include "felix/felix_client_thread_extension.hpp"

class FelixClientThreadExtension42 : public FelixClientThreadExtension {

public:
    virtual ~FelixClientThreadExtension42() {};

    /**
     * @brief Function to send a command to felix-register.
     *
     * This function takes care of establishing a connection to the remote endpoint
     * if not already available. The send request is passed to the eventloop thread.
     * The caller thread is blocked until the send operation is completed.
     *
     * @param fids The fid, typically published by felix-toflx, to which the message is addressed to.
     * @param cmd Pointer to message
     * @param cmd_args Size of message
     * @param replies For buffered mode, indicates if the buffer shall be sent right away.
     *
     * @throws FelixClientException if sending the command fails.
     */
    virtual Status send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) = 0;


    using FelixClientThreadInterface::subscribe;

    /**
     * @brief Subscribe to a list of fids.
     *
     * The remote endpoint is identified from felix-bus and a connection is
     * established if not already present. The creation of sockets and network
     * buffers is performed in the event loop thread. This function blocks the
     * caller thread until an attempt to establish the connection has started
     * or the subscription message is sent to the remote endpoint.
     *
     * If a timeout is specified using the FELIX_CLIENT_TIMEOUT property,
     * the function blocks until the subscription succeds or the timeout expires.
     * A successful subscription will be either way acknowledge by the
     * OnConnectCallback callback. A subscription is considered successful if
     * the request was received by the remote endpoint.
     *
     * @param fids The std::vector of fids to subscribe to.
     * @throws FelixClientException if the send operation fails.
     */
    virtual void subscribe(const std::vector<uint64_t>& fids) = 0;
};
