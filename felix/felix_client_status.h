#pragma once

/*! \file felix_client_status.h
    \brief Definition of message status bytes. Error flags are bitwise OR'ed.
*/

/**
 * @defgroup STATUS
 *
 * @{
 */

/** @brief Message truncated in firmware. Value: 0x1. */
#define FELIX_STATUS_FW_TRUNC (1)

/** @brief Message truncated in software (e.g. message too long). Value: 0x2. */
#define FELIX_STATUS_SW_TRUNC (2)

/** @brief Inconsistency in message structure. Value: 0x4.*/
#define FELIX_STATUS_FW_MALF (4)

/** @brief CRC error detected. Value 0x8. */
#define FELIX_STATUS_FW_CRC (8)

/** @brief Inconsistency in message decoding in software. Value 0x10.*/
#define FELIX_STATUS_SW_MALF (16)

/** @} */

/* for backward compatibility with 4.1.x */
#define FELIX_STATUS_FW_ERR (4)
