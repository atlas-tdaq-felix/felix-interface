#pragma once

#include <memory>
#include <string>
#include <vector>

#include "felix/felix_client_thread_extension421.hpp"

class FelixClientThreadExtension422 : public FelixClientThreadExtension421 {

public:
    virtual ~FelixClientThreadExtension422() {};

    /**
     * @brief Function to send a message to a remote fid without blocking.
     *
     * This function sends data in a non-blocking way from the user perspective:
     * the actual send operation is performed by the evloop thread but the
     * function returns immediately instead of blocking until the operation is
     * successfull. This function makes a copy of the data thus, when
     * the function returns, it is safe to use the data memory again.
     *
     * @note: in order to be non-blocking, this function cannot take care of
     * initializing the send connection. This means the user has to call init_send_data()
     * beforehand and  wait for the connection to be established before
     * the sending messages. Sending anyway will result in a FelixClientSendConnectionException.
     *
     * @param fid The fid, typically published by felix-toflx, to which the message is addressed to.
     * @param data Pointer to message
     * @param size Size of message
     * @param flush For buffered mode, indicates if the buffer shall be sent right away.
     *
     * @throws FelixClientException if the send operation fails.
     */
    virtual void send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) = 0;


    /**
     * @brief Function to send a group of messages to a remote fid without blocking.
     * @overload
     * This function overloads send_data_nb() allowing to send a vector of messages
     * in one send operation.
     *
     * @param fid The fid, typically published by felix-toflx, to which the messages are addressed to.
     * @param msgs Vector of pointers to messages.
     * @param sizes Vector of message sizes.
     *
     * @throws FelixClientException if the send operation fails.
     */
    virtual void send_data_nb(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) = 0;
};
