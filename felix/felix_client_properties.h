#pragma once

/*! \file felix_client_properties.h
    \brief Felix-client-thread-interface configuration parameters.
*/

/**
 * @defgroup PROPERTIES
 *
 * @{
 */

/** @brief Local IP or interface */
#define FELIX_CLIENT_LOCAL_IP_OR_INTERFACE "local_ip_or_interface"

/** @brief Log-level (trace, debug, info, warning, error, fatal) */
#define FELIX_CLIENT_LOG_LEVEL "log_level"

/** @brief Path to felix-bus directory, to be accessible by both local and remote endpoint. */
#define FELIX_CLIENT_BUS_DIR "bus_dir"

/** @brief Optional: felix-bus group name, default: FELIX. */
#define FELIX_CLIENT_BUS_GROUP_NAME "bus_group_name"

 /** @brief Optional: set to "True" to enable verbose logging in felix-bus.  */
#define FELIX_CLIENT_VERBOSE_BUS "verbose_bus"

 /** @brief Optional: subscription timeout in ms.  */
#define FELIX_CLIENT_TIMEOUT "timeout"

 /** @brief Optional, expert: override number of netio pages.  */
#define FELIX_CLIENT_NETIO_PAGES "netio_pages"

 /** @brief Optional, expert: override size of netio pages. */
#define FELIX_CLIENT_NETIO_PAGESIZE "netio_pagesize"

 /** @brief Optional: set thread affinity.  */
#define FELIX_CLIENT_THREAD_AFFINITY "thread_affinity"

/** @brief Optional, expert: enable retrieval of configuration variables from environment.  */
#define FELIX_CLIENT_READ_ENV "read_env"

/** @} */
// NOTE: Any new property here should also be added in felix-client:
// in file felix_client_thread_impl.cpp in the constructor in list propertiesList
