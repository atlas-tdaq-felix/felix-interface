#pragma once

#include <memory>
#include <string>
#include <vector>

#include "felix/felix_client_thread_extension42.hpp"


class FelixClientThreadExtension421 : public FelixClientThreadExtension42 {

public:
    virtual ~FelixClientThreadExtension421() {};

    using FelixClientThreadInterface::send_data;

    /**
     * @brief Function to send a group of messages to a remote fid.
     *
     * This function overload allows to send a set of messages in a single send
     * operation. The pointers to the messages and their sizes need to be passed
     * to this function. Otherwise it works as send_data for a single message.
     *
     * @param fid The fid, typically published by felix-toflx, to which the messages are addressed to.
     * @param msgs Vector of pointers to messages.
     * @param sizes Vector of message sizes.
     *
     * @throws FelixClientException if the send operation fails.
     */
    virtual void send_data(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) = 0;
};
