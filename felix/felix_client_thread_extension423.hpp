#pragma once

#include "felix/felix_client_thread_extension422.hpp"

class FelixClientThreadExtension423 : public FelixClientThreadExtension422 {

public:
    virtual ~FelixClientThreadExtension423() {};

    /**
     * @brief start user timer function in the eventloop thread
     *
     * @param interval interval time in ms.
     */
    virtual void user_timer_start(unsigned long interval) = 0;

    /**
     * @brief stop user timer function in the eventloop thread
     */
    virtual void user_timer_stop() = 0;

    /**
     * @brief Execute a user timer function in the eventloop thread
     *
     * This function allows to execute a user timer function in the event loop thread.
     *
     * @param on_user_timer_cb function pointer to user timer function.
     */
    virtual void callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) = 0;
};
