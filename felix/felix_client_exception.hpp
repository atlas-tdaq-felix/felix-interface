#pragma once

#include <stdexcept>

class FelixClientException: public std::runtime_error {
public:
    FelixClientException(std::string const& message) : std::runtime_error(message) {
    }
};

class FelixClientMessageTooBigException: public FelixClientException {
public:
    FelixClientMessageTooBigException() : FelixClientException("Message too big for buffer") {
    }
};

class FelixClientResourceNotAvailableException: public FelixClientException {
public:
    FelixClientResourceNotAvailableException() : FelixClientException("Resource currently not available, try again") {
    }
};

class FelixClientPartiallyCompletedException: public FelixClientException {
public:
    FelixClientPartiallyCompletedException() : FelixClientException("Operation completed partially, try again") {
    }
};

class FelixClientSendConnectionException: public FelixClientException {
public:
    FelixClientSendConnectionException() : FelixClientException("Could not establish send connection, try again") {
    }
};

class FelixClientSubscribeException: public FelixClientException {
public:
    FelixClientSubscribeException() : FelixClientException("Could not subscribe, try again") {
    }
};