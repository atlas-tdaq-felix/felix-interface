Felix-client-thread API
=======================

Felix-client-thread is a library providing a user interface for communication
with FELIX systems over the network.
Felix-client-thread exposes a handful of methods and hooks for callbacks that
abstract the complexity of the underlying layers.
Methods are function that can be called directly by the user to perform
operations such as sending a message. Callbacks are also defined by the user
and invoked asynchronously upon the occurrence of network events.

Felix-client-thread allows:

- the evolution of FELIX software without breaking compatibility with user applications
  as long as the API remains unchanged,

- the possibility, for the user, to change FELIX software release on the fly by simply
  redefining LD_LIBRARY_PATH.

The API can be extended upon user request and remains backwards compatible.

Note that the implementation underlying the felix-client-thread API spawns an
additional thread - called event loop thread - used to react to network events.
Callbacks defined by the user will be run by the event loop thread and shall be
non-blocking. The implementation underlying the felix-client-thread API is
thread-safe.



Callbacks
.........

Functions defined in the user application can be assigned as callbacks to
asynchronous events. The hooks listed below are available.

.. important:: Callbacks are executed by the event loop thread. Blocking on a callback
  prevents any other network event to be reported and impacts the sender as well.
  Locks in callbacks shall be avoided when possible or used carefully so to avoid
  deadlocks.

.. doxygentypedef:: FelixClientThreadInterface::OnDataCallback
  :no-link:

.. warning:: The message pointer reported by OnDataCallback will become invalid as
  the function returns. The reason is that, once read, network buffers are returned
  and reused to receive new messages.

.. doxygentypedef:: FelixClientThreadInterface::OnConnectCallback
  :no-link:

.. doxygentypedef:: FelixClientThreadInterface::OnDisconnectCallback
  :no-link:

.. doxygentypedef:: FelixClientThreadInterface::OnInitCallback
  :no-link:


Functions
.........

The following functions allows a user application to:

- receive data from FELIX. FELIX uses a publish/subscribe pattern: a user can
  subscribe or unsubscribe to a set of published fids.

- send data to FELIX. Messages sent to a given fid are then transferred by the
  FELIX card to the front-end

- send commands to FELIX. Commands are particular messages that are able to
  modify the registers of a FELIX card. Only a reduced set of commands is available
  to the user.

.. doxygenfunction:: FelixClientThreadInterface::subscribe
  :no-link:

.. doxygenfunction:: FelixClientThreadExtension42::subscribe
  :no-link:

.. doxygenfunction:: FelixClientThreadInterface::unsubscribe
  :no-link:

.. doxygenfunction:: FelixClientThreadInterface::send_data
  :no-link:

.. doxygenfunction:: FelixClientThreadExtension421::send_data
  :no-link:

.. doxygenfunction:: FelixClientThreadExtension422::send_data_nb(uint64_t, const std::vector<const uint8_t *>&, const std::vector<size_t>&)
  :no-link:

.. doxygenfunction:: FelixClientThreadExtension422::send_data_nb(uint64_t, const uint8_t *, size_t, bool)
  :no-link:


Configuration Parameters
........................

To initialise the communication with FELIX a set of parameters needs to be
defined. The configuration parameters are contained in the Properties map.

.. doxygentypedef:: FelixClientThreadInterface::Properties
  :no-link:

The parameters are listed below. As described, only some need to be initialised,
others are optionals or for exper use

.. doxygengroup:: PROPERTIES
  :no-link:

Error codes
...........

Each message received from the FELIX system is preceded by a "status byte" i.e.
a one-byte field that reports possible errors. The error codes are listed below
and can be OR'ed.

.. doxygengroup:: STATUS
  :no-link:
