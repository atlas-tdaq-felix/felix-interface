Use of Felix-Client-Thread
==================================

Minimal user application example
................................

A simple C++ user application is presented below.
The main header is `felix_client_thread.hpp`, while `felix_client_properties.h`
provides access to the configuration parameters and `felix_client_exception`
to exceptions.

.. code-block:: C
   :linenos:

   #include <string>
   #include <iostream>
   #include <memory>
   
   #include "felix/felix_client_properties.h"
   #include "felix/felix_client_thread.hpp"
   #include "felix/felix_client_exception.hpp"

   
   void on_init() {
     printf("on_init called\n");
   }
   
   void on_connect(uint64_t fid) {
     printf("on_connect called 0x%lx\n", fid);
   }
   
   void on_disconnect(uint64_t fid) {
     printf("on_disconnect called 0x%lx\n", fid);
   }
   
   int main(int argc, char** argv)
   {
     FelixClientThread::Config config;
     config.on_init_callback = on_init;
     config.on_connect_callback = on_connect;
     config.on_disconnect_callback = on_disconnect;
   
     config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = std::string("lo");// "127.0.0.1"
     config.property[FELIX_CLIENT_LOG_LEVEL] = "trace";
     config.property[FELIX_CLIENT_BUS_DIR] = "./bus";
     config.property[FELIX_CLIENT_BUS_GROUP_NAME] = "FELIX";
     config.property[FELIX_CLIENT_VERBOSE_BUS] = "False";
   
     try {
       std::unique_ptr<FelixClientThread> client = std::make_unique<FelixClientThread>(config);
     } catch (FelixClientException& e) {
       std::cout << e.what() << std::endl;
       return 1;
     } catch (std::runtime_error& rte) {
       std::cout << rte.what() << std::endl;
       return 2;
     }
   
     return 0;
   }


The headers and libraries are founds in each FELIX release.
The application can be build using CMake using the following `CMakeLists.txt`,
where FELIX release 05-00-02 is used as example.

.. code-block:: none
   :linenos:

   cmake_minimum_required(VERSION 3.4.3)
   project(FelixClientApp)

   set(FELIX_RELEASE /cvmfs/atlas-online-nightlies.cern.ch/felix/releases/felix-05-00-02-rm5-stand-alone/x86_64-centos9-gcc11-opt)
 
   add_executable(felix-client-app main.cpp)

   target_include_directories(felix-client-app PRIVATE ${FELIX_RELEASE}/include)
   target_link_directories(felix-client-app PRIVATE ${FELIX_RELEASE}/lib)  
   target_link_libraries(felix-client-app PRIVATE felix-client-thread)

In alternative to CMake, a simple bash script can be used to build the application.

.. code-block:: shell

   #!/usr/bin/env bash

   FELIX_RELEASE=/cvmfs/atlas-online-nightlies.cern.ch/felix/releases/felix-05-00-02-rm5-stand-alone/x86_64-centos9-gcc11-opt

   g++ -o felix-client-app \
       -I $FELIX_RELEASE/include \
       -L $FELIX_RELEASE/lib/ \
       -l felix-client-thread \
       main.cpp



Advanced application in TDAQ environment
........................................

TDAQ releases include the `felix_proxy <https://gitlab.cern.ch/atlas-tdaq-software/felix_proxy>`_
library, a wrapper of `felix-client-thread` used by SW ROD and Data Handler.

An example application that 

- implements two-way communication with FELIX,

- uses felix_proxy,

- and is built within the TDAQ environment

can be found at `https://gitlab.cern.ch/atlas-tdaq-felix-dev/felix-user-app <https://gitlab.cern.ch/atlas-tdaq-felix-dev/felix-user-app>`_.

The application functionality is detailed in its README.
